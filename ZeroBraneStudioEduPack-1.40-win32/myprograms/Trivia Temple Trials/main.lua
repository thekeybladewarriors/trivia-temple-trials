
mouse = {}

function love.load()  
   
  diceRoll = false
  pause = false
  movie = false
  general = false
  lit = false
  music = false
  videogame = false
  history = false
  move = 0
  moveLeft = false
  moveRight = false
  moveUp = false
  moveDown = false
  gamestate = "menu"
  previousState = true
  answer = false
  historyQuestionsLoad()
  literatureQuestionsLoad()
  videoGameQuestionsLoad()
  movieQuestionsLoad()
  musicQuestionsLoad()
  generalQuestionsLoad()
  repeat
  correctAnsPos = love.math.random(0,3)
  wrongAns1Pos = love.math.random(0,3)
  wrongAns2Pos = love.math.random(0,3)
  wrongAns3Pos = love.math.random(0,3)
  until (correctAnsPos ~= wrongAns1Pos and correctAnsPos ~= wrongAns2Pos and correctAnsPos ~= wrongAns3Pos and wrongAns1Pos ~= wrongAns2Pos and wrongAns1Pos ~= wrongAns3Pos and wrongAns2Pos ~= wrongAns3Pos)
love.graphics.setBackgroundColor(255, 255, 255)

--  gameBoard = love.graphics.newImage("sprites/bg.png")
  questionBorder = love.graphics.newImage("sprites/QuestionBorder.png") 
  questionPosX = 0
  questionPosY = 80
  answerBorder = love.graphics.newImage("sprites/answerBorder.png") 
  answerPosX = 0
  answerPosY = 250
  answer2Border = love.graphics.newImage("sprites/answerBorder.png") 
  answer2PosY = 350
  answer3Border = love.graphics.newImage("sprites/answerBorder.png") 
  answer3PosY = 450
  answer4Border = love.graphics.newImage("sprites/answerBorder.png") 
  answer4PosY = 550
  
  startButton = love.graphics.newImage("sprites/startButton.png") 
  
  helpButton = love.graphics.newImage("sprites/helpButton.png")  
  
  quitButton = love.graphics.newImage("sprites/quitButton.png")  
  
  pauseButton = love.graphics.newImage("sprites/pauseButton.png") 
  
  backButton = love.graphics.newImage("sprites/backButton.png") 
  questiontest = love.graphics.newImage("sprites/questionTest.png")
  vgqTest = love.graphics.newImage("sprites/VGQtest.png")
  histTest = love.graphics.newImage("sprites/HistQTest.png")
  movieTest = love.graphics.newImage("sprites/movieQtest.png")
  genTest = love.graphics.newImage("sprites/GenQtest.png")
  musicTest = love.graphics.newImage("sprites/musicQtest.png")
  litTest = love.graphics.newImage("sprites/litTest.png")
  wrong = love.graphics.newImage("sprites/wrong.png")
  correct = love.graphics.newImage("sprites/correct.png")
  tile = {}
  
    --tile[i] = love.graphics.newImage("tile"..i..".png")   -- <--- not sure on this bit, confusing as to names of tiles
    tile[0] = love.graphics.newImage("sprites/Wall Tile.png") 
    tile[1] = love.graphics.newImage("sprites/Door Tile.png") 
    tile[2] = love.graphics.newImage("sprites/Game Question Tile.png") 
    tile[3] = love.graphics.newImage("sprites/General Knowledge Tile.png") 
    tile[4] = love.graphics.newImage("sprites/History Question Tile.png") 
    tile[5] = love.graphics.newImage("sprites/Literature Question Tile.png") 
    tile[6] = love.graphics.newImage("sprites/Movie Question Tile.png") 
    tile[7] = love.graphics.newImage("sprites/Music Question Tile.png") 
    tile[8] = love.graphics.newImage("sprites/Game Question Tile Key.png") 
    tile[9] = love.graphics.newImage("sprites/General Knowledge Question Tile Key.png") 
    tile[10] = love.graphics.newImage("sprites/History Question Tile Key.png") 
    tile[11] = love.graphics.newImage("sprites/Literature Question Tile Key.png") 
    tile[12] = love.graphics.newImage("sprites/Movie Question Tile Key.png") 
    tile[13] = love.graphics.newImage("sprites/Music Question Tile Key.png") 
    tile[14] = love.graphics.newImage("sprites/Keyhole On Door Tile.png") 
    
  
  map={
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},   
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},          -- 0 = Wall (Grey) Done
    {0, 0, 0, 2, 7, 4, 0, 0, 4, 7, 5, 3, 4, 0, 5, 4, 6, 2, 4, 5, 3, 6, 7, 5, 4, 8, 0},          -- 1 = Door (Brown) Done
    {0, 0, 0, 6, 0, 11, 3, 2, 6, 0, 0, 0, 6, 0, 7, 0, 0, 3, 0, 0, 0, 4, 0, 0, 0, 6, 0},         -- 2 = Game Question Tile (Green)
    {0, 0, 0, 5, 0, 6, 0, 7, 0, 0, 7, 2, 5, 0, 2, 6, 0, 4, 7, 6, 5, 2, 0, 3, 5, 7, 0},          -- 3 = General Knowledge Question Tile (Purple)
    {0, 0, 0, 4, 0, 4, 0, 5, 3, 4, 6, 0, 7, 0, 0, 5, 0, 0, 2, 0, 0, 0, 0, 7, 0, 0, 0},          -- 4 = History Question Tile (Yellow)
    {0, 0, 0, 3, 0, 5, 3, 6, 0, 0, 0, 0, 3, 0, 7, 10, 0, 6, 5, 7, 3, 2, 6, 4, 0, 0, 0},         -- 5 = Literature Question Tile (Blue)
    {0, 0, 0, 4, 7, 2, 0, 7, 0, 9, 5, 7, 4, 0, 3, 0, 0, 2, 0, 0, 4, 0, 0, 0, 0, 0, 0},          -- 6 = Movie Question Tile (Red)
    {0, 0, 1, 6, 0, 4, 5, 3, 2, 6, 4, 0, 5, 14, 6, 2, 5, 4, 7, 0, 7, 6, 5, 2, 14, 0, 0},        -- 7 = Music Question Tile (Orange)
    {0, 0, 0, 5, 0, 6, 0, 4, 0, 0, 7, 0, 3, 0, 4, 0, 0, 0, 2, 6, 2, 0, 0, 7, 0, 0, 0},          -- 8 = Game Question Tile Key (Green)
    {0, 0, 0, 4, 0, 3, 0, 6, 2, 0, 2, 0, 2, 0, 7, 3, 2, 5, 3, 0, 5, 3, 0, 4, 0, 0, 0},          -- 9 = General Knowledge Question Tile Key (Purple)
    {0, 0, 0, 2, 6, 5, 0, 0, 7, 5, 6, 4, 7, 0, 0, 6, 0, 6, 0, 0, 0, 4, 0, 2, 0, 0, 0},          -- 10 = History Question Tile Key (Yellow)
    {0, 0, 0, 7, 0, 7, 4, 6, 3, 0, 7, 0, 3, 0, 2, 5, 0, 7, 4, 3, 12, 5, 3, 7, 6, 5, 0},         -- 11 = Literature Question Tile Key (Blue)
    {0, 0, 0, 3, 0, 3, 0, 2, 0, 0, 6, 0, 13, 0, 3, 0, 0, 0, 5, 0, 2, 0, 0, 0, 0, 3, 0},         -- 12 = Movie Question Tile Key (Red)
    {0, 0, 0, 6, 2, 4, 0, 7, 4, 5, 2, 5, 4, 0, 4, 7, 2, 3, 6, 0, 7, 4, 2, 3, 6, 4, 0},          -- 13 = Music Question Tile Key (Orange)
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},          -- 14 = Keyhole Door (Brown)
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
  }   
  
  map_w = #map[1] -- gets width of first row of map
  map_h = #map  -- gets height of map
  map_x = 0
  map_y = 0
  map_display_buffer = 2    -- buffer a tile around viewpoint so tiles don't just pop into view
  
  map_display_w = 10
  map_display_h = 10
  tile_w = 16
  tile_h = 16
    
  blankDie = love.graphics.newImage("sprites/BlankDie.png")
  dice1 = love.graphics.newImage("sprites/Dice1.png")
  dice2 = love.graphics.newImage("sprites/Dice2.png")
  dice3 = love.graphics.newImage("sprites/Dice3.png")
  dice4 = love.graphics.newImage("sprites/Dice4.png")
  dice5 = love.graphics.newImage("sprites/Dice5.png")
  dice6 = love.graphics.newImage("sprites/Dice6.png")

  gameBoardPosX = 50/2
  gameBoardPosY = -77/2

  startButtonPosX = 222/2
  startButtonPosY = 300
  
  helpButtonPosX = 222/2
  helpButtonPosY = 400
  
  quitButtonPosX = 222/2
  quitButtonPosY = 500
  
  pauseButtonPosX = 10/2
  pauseButtonPosY = 10/2
  
  backButtonPosX = 10/2
  backButtonPosY = 1140/2
  
  questTestPosX = 222/2
  questTestPosY = 200
  
  musicTestPosX = 222/2
  musicTestPosY = 100
  
  litTestPosX = 222/2
  litTestPosY = 0
  playerX = 0
  playerY = 0
  leftSquarePosX = playerX - 64
  leftSquarePosY = playerY
  
  rightSquarePosX = playerX + 64
  rightSquarePosY = playerY
  
  upSquarePosX = playerX
  upSquarePosY = playerY - 64
  
  downSquarePosX = playerX
  downSquarePosY = playerY + 64
  
  blankDiePosX = 580/2
  blankDiePosY = 1140/2
  dice1PosX = 580/2
  dice1PosY = 1140/2
  dice2PosX = 580/2
  dice2PosY = 1140/2
  dice3PosX = 580/2
  dice3PosY = 1140/2
  dice4PosX = 580/2
  dice4PosY = 1140/2
  dice5PosX = 580/2
  dice5PosY = 1140/2
  dice6PosX = 580/2
  dice6PosY = 1140/2
end

function love.update ()
  mouseX = love.mouse.getX()
  mouseY = love.mouse.getY()
  
    checkStart = CheckCollision(mouseX, mouseY, 1, 1, startButtonPosX, startButtonPosY, 144, 64)  
    checkHelp = CheckCollision(mouseX, mouseY, 1, 1, helpButtonPosX, helpButtonPosY, 144, 64)
    checkQuit = CheckCollision(mouseX, mouseY, 1, 1, quitButtonPosX, quitButtonPosY, 144, 64)
    checkPause = CheckCollision(mouseX, mouseY, 1, 1, pauseButtonPosX, pauseButtonPosY, 64, 64)
    checkBlankDie = CheckCollision(mouseX, mouseY, 1, 1, blankDiePosX, blankDiePosY, 64, 64)
    checkBackHelp = CheckCollision(mouseX, mouseY, 1, 1, backButtonPosX, backButtonPosY, 64, 64)
    checkBackMain = CheckCollision(mouseX, mouseY, 1, 1, backButtonPosX, backButtonPosY, 64, 64)
    checkMusicTest = CheckCollision(mouseX, mouseY, 1, 1, musicTestPosX, musicTestPosY, 144, 64) 
    checkLitTest = CheckCollision(mouseX, mouseY, 1, 1,  litTestPosX, litTestPosY, 144, 64)
    checkQuestTest = CheckCollision(mouseX, mouseY, 1, 1,  questTestPosX, questTestPosY, 144, 64)
    checkAnswer1 = CheckCollision(mouseX, mouseY, 1, 1, answerPosX,answerPosY,360,64)
    checkAnswer2 = CheckCollision(mouseX, mouseY, 1, 1, answerPosX,answer2PosY,360,64)
    checkAnswer3 = CheckCollision(mouseX, mouseY, 1, 1, answerPosX,answer3PosY,360,64)
    checkAnswer4 = CheckCollision(mouseX, mouseY, 1, 1, answerPosX,answer4PosY,360,64)
    if leftSquare == true then
      leftSquare = CheckCollision(mouseX, mouseY, 1, 1, leftSquarePosX, leftSquarePosY, 64, 64)
    end
    if rightSquare == true then
      rightSquare = CheckCollision(mouseX, mouseY, 1, 1, rightSquarePosX, rightSquarePosY, 64, 64)
    end
    if upSquare == true then
      upSquare = CheckCollision(mouseX, mouseY, 1, 1, upSquarePosX, upSquarePosY, 64, 64)
    end
    if downSquare == true then
      downSquare = CheckCollision(mouseX, mouseY, 1, 1, downSquarePosX, downSquarePosY, 64, 64)
    end
    if Move == 0 then
      diceRoll = false
      -- this is where questions would then be asked and the turn would end or roll again
    end
end
 
function love.draw()
  game_screen()
   if gamestate == "menu" then        
    love.graphics.draw(startButton, startButtonPosX, startButtonPosY)
    love.graphics.draw(helpButton, helpButtonPosX, helpButtonPosY)
    love.graphics.draw(quitButton, quitButtonPosX, quitButtonPosY) 
    love.graphics.draw(questiontest, questTestPosX, questTestPosY)
  elseif (gamestate == "game" and pause == false) then
    --love.graphics.setBackgroundColor(128, 128, 128)
    --love.graphics.draw(gameBoard, gameBoardPosX, gameBoardPosY)
    draw_map()
    love.graphics.draw(pauseButton, pauseButtonPosX, pauseButtonPosY)
    if diceRoll == false then
      love.graphics.draw(blankDie, blankDiePosX, blankDiePosY)    
    end
    if diceRoll == true and DiceRoll == 1 then
      love.graphics.draw(dice1, dice1PosX, dice1PosY) 
    end
    if diceRoll == true and DiceRoll == 2 then
      love.graphics.draw(dice2, dice2PosX, dice2PosY) 
    end
    if diceRoll == true and DiceRoll == 3 then
      love.graphics.draw(dice3, dice3PosX, dice3PosY) 
    end
    if diceRoll == true and DiceRoll == 4 then
      love.graphics.draw(dice4, dice4PosX, dice4PosY) 
    end
    if diceRoll == true and DiceRoll == 5 then
      love.graphics.draw(dice5, dice5PosX, dice5PosY) 
    end
    if diceRoll == true and DiceRoll == 6 then
      love.graphics.draw(dice6, dice6PosX, dice6PosY) 
    end
    
  elseif (gamestate == "instructions") then 
    love.graphics.draw(backButton, backButtonPosX, backButtonPosY)
    
  elseif (gamestate == "game" and pause == true) then
    love.graphics.draw(backButton, backButtonPosX, backButtonPosY)
    
  elseif (gamestate == "questiontest") then
    love.graphics.draw(backButton, backButtonPosX, backButtonPosY)
    love.graphics.draw(musicTest, musicTestPosX, musicTestPosY)
    love.graphics.draw(movieTest, startButtonPosX, startButtonPosY)
    love.graphics.draw(vgqTest, helpButtonPosX, helpButtonPosY)
    love.graphics.draw(genTest, quitButtonPosX, quitButtonPosY) 
    love.graphics.draw(histTest, questTestPosX, questTestPosY)
    love.graphics.draw(litTest, litTestPosX, litTestPosY)
  elseif (gamestate == "question") then
    drawQuestions()
  elseif (gamestate == "answer") then
    if(answer == true) then
    love.graphics.draw(correct, questTestPosX, questTestPosY)
  else
    love.graphics.draw(wrong, questTestPosX, questTestPosY)
    end
  end
end

function draw_map()
  offset_x = map_x % tile_w
  offset_y = map_y % tile_h
  firstTile_x = math.floor(map_x / tile_w)
	firstTile_y = math.floor(map_y / tile_h)
  for y=1, (map_display_h + map_display_buffer) do
		for x=1, (map_display_w + map_display_buffer) do
			-- Note that this condition block allows us to go beyond the edge of the map.
			if y+firstTile_y >= 1 and y+firstTile_y <= map_h
				and x+firstTile_x >= 1 and x+firstTile_x <= map_w
			then
				love.graphics.draw(tile[map[y+map_y][x+map_x]],((x-1)*tile_w) - offset_x - tile_w/2,((y-1)*tile_h) - offset_y - tile_h/2)
			end
		end
	end
 end
 
function love.mousepressed(mx, my, button)
  if (button == 1 and checkStart and gamestate == "menu") then    
    gamestate = "game"
  end
  if (button == 1 and checkPause and gamestate == "game") then    
    gamestate = "pause"
  end
  if (button == 1 and checkHelp and gamestate == "menu") then    
    gamestate = "instructions"
  end
  if (button == 1 and checkQuit and gamestate == "menu") then    
    love.event.quit()
  end
  if (button == 1 and checkBackHelp and (gamestate == "instructions" or gamestate == "questiontest")) then    
  gamestate = "menu"
  end 
  if (button == 1 and checkBackMain and pause == true) then    
  gamestate = "menu"
  end
  if (button == 1 and leftSquare and moveLeft == true) then    
    playerX = playerX - 64
    Move = Move - 1
  end
  if (button == 1 and rightSquare and moveRight == true) then    
    playerX = playerX + 64
    Move = Move - 1
  end
  if (button == 1 and upSquare and moveUp == true) then    
    playerY = playerY - 64
    Move = Move - 1
  end
  if (button == 1 and downSquare and moveDown == true) then    
    playerY = playerY + 64
    Move = Move - 1
  end
  if (button == 1 and checkBlankDie) then    
    Dice_Roll()
  end
  if (button == 1 and checkQuestTest and gamestate == "menu" and previousState == true) then
      main = false
      gamestate = "questiontest"
      previousState = false
  end
  if (button == 1 and checkQuestTest and gamestate == "questiontest" and previousState == true) then
    history = true
    questionCheck()
    gamestate = "question"
    previousState = false
  end
  if (button == 1 and checkHelp and gamestate == "questiontest" and previousState == true) then
    videogame = true
    questionCheck()
    gamestate = "question"
    previousState = false
  end
    if (button == 1 and checkLitTest and gamestate == "questiontest" and previousState == true) then
    lit = true
    questionCheck()
    gamestate = "question"
    previousState = false
  end
    if (button == 1 and checkMusicTest and gamestate == "questiontest" and previousState == true) then
    music = true
    questionCheck()
    gamestate = "question"
    previousState = false
  end
    if (button == 1 and checkStart and gamestate == "questiontest" and previousState == true) then
    movie = true
    questionCheck()
    gamestate = "question"
    previousState = false
  end
    if (button == 1 and checkQuit and gamestate == "questiontest" and previousState == true) then
    general = true
    questionCheck()
    gamestate = "question"
    previousState = false
  end
  if (button == 1 and checkAnswer1 and gamestate == "question" and previousState == true) then
    history = false
    general = false
    movie = false
    music = false
    videogame = false
    lit = false
    if (correctAnsPos == 0) then
      answer = true
    else
      answer = false
    end
    gamestate = "answer"
    previousState = false
end
  if (button == 1 and checkAnswer2 and gamestate == "question" and previousState == true) then
    history = false
    general = false
    movie = false
    music = false
    videogame = false
    lit = false
    if (correctAnsPos == 1) then
      answer = true
    else
      answer = false
    end
    gamestate = "answer"
    previousState = false
end
  if (button == 1 and checkAnswer3 and gamestate == "question" and previousState == true) then
    history = false
    general = false
    movie = false
    music = false
    videogame = false
    lit = false
    if (correctAnsPos == 2) then
      answer = true
    else
      answer = false
    end
    gamestate = "answer"
    previousState = false
end
  if (button == 1 and checkAnswer4 and gamestate == "question" and previousState == true) then
    history = false
    general = false
    movie = false
    music = false
    videogame = false
    lit = false
    if (correctAnsPos == 3) then
      answer = true
    else
      answer = false
    end
    gamestate = "answer"
    previousState = false
end
if (button == 1 and checkQuestTest and gamestate == "answer" and previousState == true) then
  gamestate = "menu"
  previousState = false
  end
end
function Dice_Roll()
  DiceRoll = love.math.random(1, 6)
  diceRoll = true
  if DiceRoll == 1 then    
  RolledNumber = 1
  Move = RolledNumber    
  end
  if DiceRoll == 2 then
  RolledNumber = 2
  Move = RolledNumber  
  end
  if DiceRoll == 3 then
  RolledNumber = 3
  Move = RolledNumber
  end
  if DiceRoll == 4 then
  RolledNumber = 4 
  Move = RolledNumber
  end
  if DiceRoll == 5 then      
  RolledNumber = 5 
  Move = RolledNumber
  end
  if DiceRoll == 6 then         
  RolledNumber = 6 
  Move = RolledNumber
  end
end
function love.mousereleased(mx,my,button)
  if (button == 1) then
      previousState = true
    else
      previousState = false
  end
end
function check_directions()
  moveLeft = true
  moveRight = true
  moveUp = true
  moveDown = true
  spaceX = playerX
  spaceY = playerY
  if spaceX == 0 then
    moveLeft = false
  end
  if spaceX == 27 then
    moveRight = false
  end
  if spaceY == 0 then
    moveUp = false
  end
  if spaceY == 17 then
    moveDown = false
  end
  if spaceX - 1 == tile[0] then
    moveLeft = false
  elseif spaceX - 1 ~= tile[0] then
    leftSquare = true
  end
  if spaceX + 1 == tile[0] then
    moveRight = false
  elseif spaceX + 1 ~= tile[0] then
    rightSquare = true
  end
  if spaceY - 1 == tile[0] then
    moveUp = false
  elseif spaceY - 1 ~= tile[0] then
    upSquare = true
  end
  if spaceY + 1 == tile[0] then
    moveDown = false
  elseif spaceY + 1 ~= tile[0] then
    downSquare = true
end
end
function game_screen() 

end

function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1                 
end
function historyQuestionsLoad()
history_Q_file = assert(io.open('questionfiles/HistoryQuestions.txt'))
  
  historyQ = {
    Q,
    RA,
    WA1,
    WA2,
    WA3
    }
  hQrow = 0
  i = 0
  for line in history_Q_file:lines() do
    if i == 0 then
    Question = line
  elseif i == 1 then
    rightAnswer = line
  elseif i == 2 then
    wrongAnswer1 = line
  elseif i == 3 then
    wrongAnswer2 = line
  elseif i == 4 then
    wrongAnswer3 = line
  elseif i == 5 then
  table.insert(historyQ,hQrow, {Q = Question, RA = rightAnswer, WA1 = wrongAnswer1, WA2 = wrongAnswer2, WA3 = wrongAnswer3})
  hQrow = hQrow + 1
end
i = i + 1
if i == 6 then
  i = 0
end
end
io.close(history_Q_file)
end
function videoGameQuestionsLoad()
videoGame_Q_file = assert(io.open('questionfiles/VideogameQuestions.txt'))
  
  videoGameQ = {
    Q,
    RA,
    WA1,
    WA2,
    WA3
    }
  vgQrow = 0
  i = 0
  for line in videoGame_Q_file:lines() do
    if i == 0 then
    Question = line
  elseif i == 1 then
    rightAnswer = line
  elseif i == 2 then
    wrongAnswer1 = line
  elseif i == 3 then
    wrongAnswer2 = line
  elseif i == 4 then
    wrongAnswer3 = line
  elseif i == 5 then
  table.insert(videoGameQ,vgQrow, {Q = Question, RA = rightAnswer, WA1 = wrongAnswer1, WA2 = wrongAnswer2, WA3 = wrongAnswer3})
  vgQrow = vgQrow + 1
end
i = i + 1
if i == 6 then
  i = 0
end
end
io.close(videoGame_Q_file)
end
function literatureQuestionsLoad()
literature_Q_file = assert(io.open('questionfiles/literatureQuestions.txt'))
  
  literatureQ = {
    Q,
    RA,
    WA1,
    WA2,
    WA3
    }
  lQrow = 0
  i = 0
  for line in literature_Q_file:lines() do
  if i == 0 then
    Question = line
  elseif i == 1 then
    rightAnswer = line
  elseif i == 2 then
    wrongAnswer1 = line
  elseif i == 3 then
    wrongAnswer2 = line
  elseif i == 4 then
    wrongAnswer3 = line
  elseif i == 5 then
  table.insert(literatureQ,lQrow, {Q = Question, RA = rightAnswer, WA1 = wrongAnswer1, WA2 = wrongAnswer2, WA3 = wrongAnswer3})
  lQrow = lQrow + 1
end
i = i + 1
if i == 6 then
  i = 0
end
end
io.close(literature_Q_file)
end
function movieQuestionsLoad()
movie_Q_file = assert(io.open('questionfiles/movieQuestions.txt'))
  
  movieQ = {
    Q,
    RA,
    WA1,
    WA2,
    WA3
    }
  mQrow = 0
  i = 0
  for line in movie_Q_file:lines() do
    if i == 0 then
    Question = line
  elseif i == 1 then
    rightAnswer = line
  elseif i == 2 then
    wrongAnswer1 = line
  elseif i == 3 then
    wrongAnswer2 = line
  elseif i == 4 then
    wrongAnswer3 = line
  elseif i == 5 then
  table.insert(movieQ,mQrow, {Q = Question, RA = rightAnswer, WA1 = wrongAnswer1, WA2 = wrongAnswer2, WA3 = wrongAnswer3})
  mQrow = mQrow + 1
  end
i = i + 1
if i == 6 then
  i = 0
end
end
io.close(movie_Q_file)
end
function musicQuestionsLoad()
music_Q_file = assert(io.open('questionfiles/musicQuestions.txt'))
  
  musicQ = {
    Q,
    RA,
    WA1,
    WA2,
    WA3
    }
  musicQrow = 0
  i = 0
  for line in music_Q_file:lines() do
    if i == 0 then
    Question = line
  elseif i == 1 then
    rightAnswer = line
  elseif i == 2 then
    wrongAnswer1 = line
  elseif i == 3 then
    wrongAnswer2 = line
  elseif i == 4 then
    wrongAnswer3 = line
  elseif i == 5 then
  table.insert(musicQ,musicQrow, {Q = Question, RA = rightAnswer, WA1 = wrongAnswer1, WA2 = wrongAnswer2, WA3 = wrongAnswer3})
  musicQrow = musicQrow + 1
end
i = i + 1
if i == 6 then
  i = 0
end
end
io.close(music_Q_file)
end
function generalQuestionsLoad()
  
general_Q_file = assert(io.open('questionfiles/generalQuestions.txt'))
  
  generalQ = {
    Q,
    RA,
    WA1,
    WA2,
    WA3
    }
  generalQrow = 0
  i = 0
  for line in general_Q_file:lines() do
    if i == 0 then
    Question = line
  elseif i == 1 then
    rightAnswer = line
  elseif i == 2 then
    wrongAnswer1 = line
  elseif i == 3 then
    wrongAnswer2 = line
  elseif i == 4 then
    wrongAnswer3 = line
  elseif i == 5 then
  table.insert(generalQ,generalQrow, {Q = Question, RA = rightAnswer, WA1 = wrongAnswer1, WA2 = wrongAnswer2, WA3 = wrongAnswer3})
  generalQrow = generalQrow + 1
  end
i = i + 1
if i == 6 then
  i = 0
end
end
io.close(general_Q_file)
end
function questionCheck()
  questionNo = love.math.random(0,18)
  if (history == true) then
    question = historyQ[questionNo].Q
    correctAns  = historyQ[questionNo].RA
    wrongAns1 = historyQ[questionNo].WA1
    wrongAns2 = historyQ[questionNo].WA2
    wrongAns3 = historyQ[questionNo].WA3
  elseif (videogame == true) then
    question = videoGameQ[questionNo].Q
    correctAns  = videoGameQ[questionNo].RA
    wrongAns1 = videoGameQ[questionNo].WA1
    wrongAns2 = videoGameQ[questionNo].WA2
    wrongAns3 = videoGameQ[questionNo].WA3
  elseif (movie == true) then
    question = movieQ[questionNo].Q
    correctAns  = movieQ[questionNo].RA
    wrongAns1 = movieQ[questionNo].WA1
    wrongAns2 = movieQ[questionNo].WA2
    wrongAns3 = movieQ[questionNo].WA3
  elseif (lit == true) then
    question = literatureQ[questionNo].Q
    correctAns  = literatureQ[questionNo].RA
    wrongAns1 = literatureQ[questionNo].WA1
    wrongAns2 = literatureQ[questionNo].WA2
    wrongAns3 = literatureQ[questionNo].WA3
  elseif (music == true) then
    question = musicQ[questionNo].Q
    correctAns  = musicQ[questionNo].RA
    wrongAns1 = musicQ[questionNo].WA1
    wrongAns2 = musicQ[questionNo].WA2
    wrongAns3 = musicQ[questionNo].WA3
  elseif (general == true) then
    question = generalQ[questionNo].Q
    correctAns  = generalQ[questionNo].RA
    wrongAns1 = generalQ[questionNo].WA1
    wrongAns2 = generalQ[questionNo].WA2
    wrongAns3 =generalQ[questionNo].WA3
  end
  repeat
  correctAnsPos = love.math.random(0,3)
  wrongAns1Pos = love.math.random(0,3)
  wrongAns2Pos = love.math.random(0,3)
  wrongAns3Pos = love.math.random(0,3)
  until (correctAnsPos ~= wrongAns1Pos and correctAnsPos ~= wrongAns2Pos and correctAnsPos ~= wrongAns3Pos and wrongAns1Pos ~= wrongAns2Pos and wrongAns1Pos ~= wrongAns3Pos and wrongAns2Pos ~= wrongAns3Pos)
end
function drawQuestions()

  love.graphics.draw(questionBorder,questionPosX,questionPosY)
  love.graphics.printf(question,questionPosX + 20,questionPosY + 10,340)
  love.graphics.draw(answerBorder,answerPosX,answerPosY)
  love.graphics.draw(answer2Border,answerPosX,answer2PosY)
  love.graphics.draw(answer3Border,answerPosX,answer3PosY)
  love.graphics.draw(answer4Border,answerPosX,answer4PosY)
  if (correctAnsPos == 0) then
    love.graphics.printf(correctAns, answerPosX + 20, answerPosY + 10,340)
  elseif (wrongAns1Pos == 0) then
    love.graphics.printf(wrongAns1, answerPosX + 20, answerPosY + 10,340)
  elseif (wrongAns2Pos == 0) then
    love.graphics.printf(wrongAns2, answerPosX + 20, answerPosY + 10,340)
  elseif (wrongAns3Pos == 0) then
    love.graphics.printf(wrongAns3, answerPosX + 20, answerPosY + 10,340)
  end
  if (correctAnsPos == 1) then
    love.graphics.printf(correctAns, answerPosX + 20, answer2PosY + 10,340)
  elseif (wrongAns1Pos == 1) then
    love.graphics.printf(wrongAns1, answerPosX + 20, answer2PosY + 10,340)
  elseif (wrongAns2Pos == 1) then
    love.graphics.printf(wrongAns2, answerPosX + 20, answer2PosY + 10,340)
  elseif (wrongAns3Pos == 1) then
    love.graphics.printf(wrongAns3, answerPosX + 20, answer2PosY + 10,340)
  end
  if (correctAnsPos == 2) then
    love.graphics.printf(correctAns, answerPosX + 20, answer3PosY + 10,340)
  elseif (wrongAns1Pos == 2) then
    love.graphics.printf(wrongAns1, answerPosX + 20, answer3PosY + 10,340)
  elseif (wrongAns2Pos == 2) then
    love.graphics.printf(wrongAns2, answerPosX + 20, answer3PosY + 10,340)
  elseif (wrongAns3Pos == 2) then
    love.graphics.printf(wrongAns3, answerPosX + 20, answer3PosY + 10,340)
  end
  if (correctAnsPos == 3) then
    love.graphics.printf(correctAns, answerPosX + 20, answer4PosY + 10,340)
  elseif (wrongAns1Pos == 3) then
    love.graphics.printf(wrongAns1, answerPosX + 20, answer4PosY + 10,340)
  elseif (wrongAns2Pos == 3) then
    love.graphics.printf(wrongAns2, answerPosX + 20, answer4PosY + 10,340)
  elseif (wrongAns3Pos == 3) then
    love.graphics.printf(wrongAns3, answerPosX + 20, answer4PosY + 10,340)
  end
  end